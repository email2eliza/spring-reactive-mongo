package com.target.retail.service

import com.target.retail.domain.PriceDetail
import com.target.retail.repository.PriceDetailRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import reactor.core.publisher.*
import reactor.util.function.Tuple2


@Service
class ProductAndPriceDetailService {

    @Autowired
    ProductService productsService

    @Autowired
    PriceDetailRepository priceDetailRepository

    Mono<ResponseEntity<Map>> getProductAndPriceDetail(String productId) {
        return Mono.zip(
                productsService.getProductDetails(productId),
                priceDetailRepository.findById(productId)
        ).flatMap({
            Tuple2<Map, PriceDetail> tuple2 ->
                Map productDetailResponse = tuple2.getT1()
                PriceDetail priceDetailResponse = tuple2.getT2()

                return Mono.just([
                        id           : productId,
                        name         : productDetailResponse.name,
                        current_price: [
                                value        : priceDetailResponse.price,
                                currency_code: priceDetailResponse.currencyCode
                        ]
                ])

        }).map({
            Map priceDetail -> ResponseEntity.ok(priceDetail)
        }).defaultIfEmpty(ResponseEntity.notFound().build())
    }

    Mono<ResponseEntity<PriceDetail>> updatePriceDetails(String productId, Map requestBody) {
        Mono<PriceDetail> priceDetail = priceDetailRepository.findById(productId)
        priceDetail.flatMap({ PriceDetail existingPriceDetail ->
            existingPriceDetail.price = requestBody.current_price.value
            existingPriceDetail.currencyCode = requestBody.current_price.currency_code
            return priceDetailRepository.save(existingPriceDetail)
        }).map({
            PriceDetail updatedPriceDetail -> new ResponseEntity<>(updatedPriceDetail, HttpStatus.OK)
        }).defaultIfEmpty(ResponseEntity.notFound().build())
    }

    Mono<ResponseEntity> savePriceDetails(String productId, Map requestBody) {
        PriceDetail priceDetailToSave = new PriceDetail(productId: productId, price: requestBody.price, currencyCode: requestBody.currencyCode)
        return priceDetailRepository.save(priceDetailToSave).map({
            PriceDetail savedPriceDetail -> ResponseEntity.ok().build()
        })
    }
}
