package com.target.retail.config

import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoClients
import com.target.retail.repository.PriceDetailRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories

@Configuration
@EnableReactiveMongoRepositories(basePackageClasses = PriceDetailRepository)
class MongoConfig extends AbstractReactiveMongoConfiguration {

    @Override
    MongoClient reactiveMongoClient() {
        return MongoClients.create(System.getenv().getOrDefault("MONGODB_CONNECTION_STRING", "mongodb://localhost:27017"))
    }

    @Override
    protected String getDatabaseName() {
        return 'test'
    }

    @Bean
    ReactiveMongoTemplate reactiveMongoTemplate() {
        return new ReactiveMongoTemplate(reactiveMongoClient(), getDatabaseName())
    }
}
