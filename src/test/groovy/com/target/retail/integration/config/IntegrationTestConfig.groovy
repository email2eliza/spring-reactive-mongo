package com.target.retail.integration.config

import com.target.retail.service.ProductAndPriceDetailService
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import spock.mock.DetachedMockFactory

@TestConfiguration
@SpringBootTest
class IntegrationTestConfig {

    private DetachedMockFactory factory = new DetachedMockFactory()

    @Bean
    ProductAndPriceDetailService productAndPriceDetailService() {
        factory.Mock(ProductAndPriceDetailService)
    }

}
