FROM tomcat:8.5-jre8-alpine

VOLUME /tmp

MAINTAINER Elizabeth Thomas <email2eliza@gmail.com>

RUN chown -R root /usr/local/* && \
    rm -rf /usr/local/tomcat/webapps/ROOT

COPY build/libs/*.war /usr/local/tomcat/webapps/ROOT.war

COPY src/main/resources /usr/local/tomcat/conf/resources

EXPOSE 8080

CMD ["catalina.sh", "run"]


