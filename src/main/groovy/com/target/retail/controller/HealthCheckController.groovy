package com.target.retail.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class HealthCheckController {

    @Value('${spring.application.name}')
    String applicationName

    @Value('${spring.application.version}')
    String applicationVersion

    @GetMapping(value = '/')
    Mono<Map> getHealthCheck() {
        return Mono.just([
                name: applicationName,
                version: applicationVersion
        ])
    }
}
