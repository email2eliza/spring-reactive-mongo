package com.target.retail.controller

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import spock.lang.Specification


class HealthCheckControllerSpec extends Specification {

    @Subject
    HealthCheckController healthCheckController

    @Collaborator
    String applicationName = 'some application'

    @Collaborator
    String applicationVersion = 'some version'

    def 'test that health check is available'() {
        expect:
        healthCheckController.getHealthCheck().block() == [name: applicationName, version: applicationVersion]
    }
}
