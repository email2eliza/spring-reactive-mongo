package com.target.retail.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.target.retail.domain.PriceDetail
import com.target.retail.integration.config.IntegrationTestConfig
import com.target.retail.service.ProductAndPriceDetailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import reactor.core.publisher.Mono
import spock.lang.Specification

@SpringBootTest
@AutoConfigureMockMvc
@Import([IntegrationTestConfig])
class ProductControllerIntegrationSpec extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ProductAndPriceDetailService productAndPriceDetailServiceMock

    ObjectMapper objectMapper = new ObjectMapper()

    def "get product and price details"() {
        setup:
        String productId = '1234567'
        Map expectedResponse = [
                id           : productId,
                title        : 'some product',
                current_price: [
                        value        : 99.99,
                        currency_code: 'USD'
                ]
        ]

        when:
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/products/{productId}", productId)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful()).andReturn()

        then:
        productAndPriceDetailServiceMock.getProductAndPriceDetail(productId) >> Mono.just(expectedResponse)
        noExceptionThrown()

        when:
        def resultingJson = mvcResult.asyncResult

        then:
        resultingJson == expectedResponse

    }

    def "updates price details"() {
        given:
        String productId = '1234567'
        Map requestBody = [
                name: 'some product',
                current_price: [
                        value: 83.49,
                        currency_code: 'USD'
                ]
        ]
        PriceDetail expectedResponse = new PriceDetail(productId: productId, currencyCode: 'USD', price: 83.49)

        when:
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.put("/products/{productId}", productId).content(objectMapper.writeValueAsBytes(requestBody))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful()).andReturn()

        then:
        productAndPriceDetailServiceMock.updatePriceDetails(productId, requestBody) >> Mono.just(expectedResponse)
        noExceptionThrown()

        when:
        def resultingJson = mvcResult.asyncResult

        then:
        resultingJson == expectedResponse
    }

    def "creates price details"() {
        given:
        String productId = '1234567'
        Map requestBody = [
                price: 34.99,
                currencyCode: 'USD'
        ]

        when:
        mockMvc.perform(
                MockMvcRequestBuilders.post("/products/{productId}/price", productId).content(objectMapper.writeValueAsBytes(requestBody))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful()).andReturn()

        then:
        productAndPriceDetailServiceMock.savePriceDetails(productId, requestBody) >> Mono.just(ResponseEntity.ok())
        noExceptionThrown()
    }
}
