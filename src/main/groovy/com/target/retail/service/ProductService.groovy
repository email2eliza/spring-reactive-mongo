package com.target.retail.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono

@Service
class ProductService {

    @Autowired
    WebClient productServiceClient

    @Value('${productService.pathTemplate}')
    String pathTemplate

    Mono<Map> getProductDetails(String productId) {
        WebClient.RequestBodyUriSpec request = productServiceClient.get().uri(pathTemplate, productId)
        return request.retrieve().bodyToMono(Map).map({
            Map response -> return [
                    id: productId,
                    name: response.product.item.product_description.title
            ]
        })
    }
}
