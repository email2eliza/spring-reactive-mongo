package com.target.retail.service

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.target.retail.domain.PriceDetail
import com.target.retail.repository.PriceDetailRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import reactor.core.publisher.Mono
import spock.lang.Ignore
import spock.lang.Specification


class ProductAndPriceDetailServiceSpec extends Specification {

    @Subject
    ProductAndPriceDetailService productAndPriceDetailService

    @Collaborator
    ProductService productService = Mock(ProductService)

    @Collaborator
    PriceDetailRepository priceDetailRepository = Mock(PriceDetailRepository)

    def "service aggregates response from products service and price detail repository"() {
        given:
        String productId = 'some product id'
        Map productDetailsResponse = [
                id  : productId,
                name: 'some title'
        ]
        Map priceDetailsResponse = [
                productId   : productId,
                price       : 25.99,
                currencyCode: 'USD'
        ]
        Map expectedResult = [
                id           : productId,
                name         : 'some title',
                current_price: [
                        value        : 25.99,
                        currency_code: 'USD'
                ]
        ]

        when:
        ResponseEntity<Map> actualResult = productAndPriceDetailService.getProductAndPriceDetail(productId).block()

        then:
        1 * productService.getProductDetails(productId) >> Mono.just(productDetailsResponse)
        1 * priceDetailRepository.findById(productId) >> Mono.just(priceDetailsResponse)
        0 * _

        actualResult.statusCode == HttpStatus.OK
        actualResult.body == expectedResult
    }

    def "service updates product's price in price detail repository"() {
        given:
        String productId = 'some product id'
        Map inputRequest = [
                id: productId,
                name: 'some title',
                current_price: [
                        value: 13.49,
                        currency_code: 'INR'
                ]
        ]
        PriceDetail existingPriceDetail = new PriceDetail(
                productId: productId,
                price: 25.99,
                currencyCode: 'USD'
        )

        PriceDetail priceDetail = new PriceDetail(
                productId: productId,
                price: 13.49,
                currencyCode: 'INR'
        )

        when:
        ResponseEntity<PriceDetail> updatedPriceDetail = productAndPriceDetailService.updatePriceDetails(productId, inputRequest).block()

        then:
        1 * priceDetailRepository.findById(productId) >> Mono.just(existingPriceDetail)
        1 * priceDetailRepository.save(_) >> Mono.just(priceDetail)
        0 * _

        updatedPriceDetail.statusCode == HttpStatus.OK
        updatedPriceDetail.body == priceDetail
    }

    def "service saves price details for a given product in price detail repository"() {
        given:
        String productId = 'some productId'
        Map requestBody = [
                price: 56.78,
                currencyCode: 'some currency'
        ]
        PriceDetail priceDetail = new PriceDetail(productId: productId, price: 56.78, currencyCode: 'some currency')

        when:
        ResponseEntity savedPriceDetail = productAndPriceDetailService.savePriceDetails(productId, requestBody).block()

        then:
        1 * priceDetailRepository.save(_) >> Mono.just(priceDetail)

        savedPriceDetail.statusCode == HttpStatus.OK
    }
}
