# Retail Demo Application

[<img src = https://gitlab.com/email2eliza/spring-reactive-mongo/badges/master/build.svg>](https://gitlab.com/email2eliza/spring-reactive-mongo/pipelines)
[<img src = https://gitlab.com/email2eliza/spring-reactive-mongo/badges/master/coverage.svg>](https://email2eliza.gitlab.io/spring-reactive-mongo/)


This application serves as a Proof-Of-Concept for an aggregation service API (aggregates data from multiple sources and serves up the data)
This application uses
* Spring reactor framework to asynchronously aggregate data between the different sources in a non-blocking manner
* MongoDB as the NoSQL store

This application supports the following endpoints
* GET /products/{productId} to aggregate product details from RedSKY API and price details from NoSQL Store for a given product.
* PUT /products/{productId} with a request body in the following format to update price details of a given product in NoSQL Store.

    *Sample PUT Request Body*
    ```
    {
        "name": "Product Title",
        "current_price": {
            "value": 99.99,
            "currency_code": "USD"
        }
    }
    ```
    Note: Currently the PUT request doesn't update the price if price details are not found in the NoSQL store but instead returns a 404 in those scenarios. There is a POST request with similar request body as that of PUT request to create/insert price details in the NoSQL Store.
    
* POST /products/{productId}/price with a request body in the following format to create price details of a given product in NoSQL Store.
     *Sample POST Request Body*
    ```
    {
        "price": 99.99,
        "currencyCode": "USD"

    }
    ```

# Usage
This application is deployed in Google Kubernetes Engine (GKE) on a cluster with 3 nodes. The GKE related configuration files are available under `kubernetes` folder of this project. Each of this file can be applied on the connected cluster using `kubectl apply -f <fileName>`
MongoDB ReplicaSet is installed on GKE using Helm Charts.   

The application can be accessed under [here](http://104.197.209.69:8080). The APIs can be accessed via [swagger](http://104.197.209.69:8080/swagger-ui.html). Alternatively, The Postman [collection JSON](https://gitlab.com/email2eliza/spring-reactive-mongo/blob/master/postman/SpringRetailDemo.postman_collection.json) found in the given link can be imported to Postman application to try out the different endpoints given by the application.

# Local Setup
1. Clone the project
2. Run the NoSQL MongoDB using the following command
    * Download the mongo image using `docker pull mongo`
    * Run the mongo image using `docker run -d -p 27017:27017 --name mongodb mongo`
3. Run the Retail Application with an environment variable of `MONGODB_CONNECTION_STRING` with value of the local mongodb running, `mongodb://localhost:27017` in this case. Alternatively, there is a Dockerfile that can be built and run to get a running version of the application.
4. Access the GET, PUT and POST endpoints using Swagger, POSTMAN or curl commands



