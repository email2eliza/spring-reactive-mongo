package com.target.retail.controller

import com.target.retail.domain.PriceDetail
import com.target.retail.service.ProductAndPriceDetailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class ProductController {

    @Autowired
    ProductAndPriceDetailService productsAndPriceDetailsService

    @GetMapping(value = "/products/{productId}")
    Mono<ResponseEntity<Map>> getProductAndPriceDetails(@PathVariable(value = "productId") String productId) {
        return productsAndPriceDetailsService.getProductAndPriceDetail(productId)
    }

    @PutMapping(value = "/products/{productId}")
    Mono<ResponseEntity<PriceDetail>> updatePriceDetails(@PathVariable(value = "productId") String productId,
                                                         @RequestBody Map priceDetail) {
        return productsAndPriceDetailsService.updatePriceDetails(productId, priceDetail)
    }

    @PostMapping(value = "/products/{productId}/price")
    Mono<ResponseEntity> savePriceDetails(@PathVariable(value = "productId") String productId, @RequestBody Map priceDetail) {
        return productsAndPriceDetailsService.savePriceDetails(productId, priceDetail)
    }
}
