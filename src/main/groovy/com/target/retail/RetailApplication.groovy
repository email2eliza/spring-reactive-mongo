package com.target.retail

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.reactive.function.client.WebClient

@SpringBootApplication
class RetailApplication {

	static void main(String[] args) {
		SpringApplication.run RetailApplication, args
	}

	@Value('${productServiceClient.baseUrl}')
	String productServiceClientUrl

	@Bean
	WebClient productServiceClient() {
		return WebClient.builder().baseUrl(productServiceClientUrl).build()
	}
}
