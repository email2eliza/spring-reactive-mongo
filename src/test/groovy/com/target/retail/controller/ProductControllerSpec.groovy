package com.target.retail.controller

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.target.retail.domain.PriceDetail
import com.target.retail.service.ProductAndPriceDetailService
import com.target.retail.service.ProductService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import reactor.core.publisher.Mono
import spock.lang.Ignore
import spock.lang.Specification

class ProductControllerSpec extends Specification {

    @Subject
    ProductController productController

    @Collaborator
    ProductAndPriceDetailService productAndPriceDetailService = Mock(ProductAndPriceDetailService)

    def "test getProductAndPriceDetails aggregates product and price details for a given product"() {
        given:
        String productId = 'some product id'
        Map expectedResult = [
                id  : 'some product id',
                name: 'some title'
        ]

        when:
        ResponseEntity<Map> actualResult = productController.getProductAndPriceDetails(productId).block()

        then:
        1 * productAndPriceDetailService.getProductAndPriceDetail(productId) >> Mono.just(ResponseEntity.ok(expectedResult))

        actualResult.statusCode == HttpStatus.OK
        actualResult.body == expectedResult
    }

    def "test updatePriceDetail for a given product"() {
        given:
        String productId = '15643793'
        Map requestBody = [
                id           : productId,
                name         : 'some product name',
                current_price: [
                        value       : 25.99,
                        currencyCode: 'USD'
                ]
        ]
        PriceDetail expectedPriceDetail = new PriceDetail(
                productId: productId,
                price: 25.99,
                currencyCode: 'USD'
        )

        when:
        ResponseEntity<PriceDetail> updatedPriceDetail = productController.updatePriceDetails(productId, requestBody).block()

        then:
        1 * productAndPriceDetailService.updatePriceDetails(productId, requestBody) >> Mono.just(ResponseEntity.ok(expectedPriceDetail))

        updatedPriceDetail.statusCode == HttpStatus.OK
        updatedPriceDetail.body == expectedPriceDetail
    }

    def 'test savePriceDetail for a given product'() {
        given:
        String productId = 'some productId'
        Map requestBody = [
                price: 99.99,
                currencyCode: 'some currency code'
        ]

        when:
        ResponseEntity savedPrice = productController.savePriceDetails(productId, requestBody).block()

        then:
        1 * productAndPriceDetailService.savePriceDetails(productId, requestBody) >> Mono.just(new ResponseEntity<>(HttpStatus.OK))

        savedPrice.statusCode == HttpStatus.OK
    }
}
