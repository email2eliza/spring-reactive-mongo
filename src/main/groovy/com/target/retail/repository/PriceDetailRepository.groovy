package com.target.retail.repository

import com.target.retail.domain.PriceDetail
import org.springframework.data.mongodb.repository.ReactiveMongoRepository


interface PriceDetailRepository extends ReactiveMongoRepository <PriceDetail, String>{

}