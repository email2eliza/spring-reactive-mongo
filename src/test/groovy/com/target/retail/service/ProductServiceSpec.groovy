package com.target.retail.service

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import spock.lang.Specification

class ProductServiceSpec extends Specification {

    @Subject
    ProductService productService

    @Collaborator
    WebClient productServiceClient = Mock(WebClient)

    @Collaborator
    String pathTemplate = "/somePathTemplate/{productId}"

    def 'test that product details are fetched'() {
        given:
        String productId = 'some product id'
        WebClient.RequestHeadersUriSpec requestHeadersUriSpec = Mock(WebClient.RequestHeadersUriSpec)
        WebClient.RequestBodyUriSpec requestBodyUriSpec = Mock(WebClient.RequestBodyUriSpec)
        WebClient.ResponseSpec responseSpec = Mock(WebClient.ResponseSpec)
        Map productResponse = [
                product: [
                        item: [
                                product_description: [
                                        title: 'some title'
                                ]
                        ]
                ]
        ]
        Map expectedResult = [
                id: productId,
                name: 'some title'
        ]

        when:
        Map actualResult = productService.getProductDetails(productId).block()

        then:
        _ * productServiceClient.get() >> requestHeadersUriSpec
        _ * requestHeadersUriSpec.uri(pathTemplate, productId) >> requestBodyUriSpec
        _ * requestBodyUriSpec.retrieve() >> responseSpec
        _ * responseSpec.bodyToMono(Map) >> Mono.just(productResponse)

        actualResult == expectedResult
    }
}
