package com.target.retail.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.config.ResourceHandlerRegistry
import org.springframework.web.reactive.config.WebFluxConfigurer
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfig implements WebFluxConfigurer {

    static final String CONTROLLER_PACKAGE = 'com.target.retail'

    @Bean
    Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo())
        .forCodeGeneration(true)
        .ignoredParameterTypes(MetaClass)
        .select()
        .apis(RequestHandlerSelectors.basePackage(CONTROLLER_PACKAGE))
        .build()
    }

    private static ApiInfo apiInfo(){
        return new ApiInfoBuilder()
        .title('Retail APIs')
        .description('Retail APIs')
        .license('Apache License Version 2.0')
        .version('2.0')
        .build()
    }

    @Override
    void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler('/swagger-ui.html**')
        .addResourceLocations('classpath:/META-INF/resources/swagger-ui.html')

        registry.addResourceHandler('/webjars/**')
        .addResourceLocations('classpath:/META-INF/resources/webjars/')
    }
}
