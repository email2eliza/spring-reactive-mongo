package com.target.retail.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Document(collection = 'priceDetails')
class PriceDetail {
    @Id
    String productId

    @NotNull
    Double price

    @NotBlank
    String currencyCode = 'USD'
}
